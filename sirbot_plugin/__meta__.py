DATA = {
    # Ordered by commit log entries
    "author": ('Ovv <ovv@outlook.com>, '),
    "author_email": 'pythondev.slack@gmail.com',
    "copyright": 'Copyright 2016 Python Developers Community',
    "description": 'Plugins for the good Sir Bot a lot',
    "license": 'MIT',
    "name": 'sirbot_plugins',
    "url": 'https://gitlab.com/PythonDevCommunity/sirbot-plugin',
    # Versions should comply with PEP440. For a discussion on
    # single-sourcing the version across setup.py and the project code,
    # see http://packaging.python.org/en/latest/tutorial.html#version
    "version": '0.0.1',
}
